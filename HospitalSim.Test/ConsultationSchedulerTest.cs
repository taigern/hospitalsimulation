﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HospitalSimConsole.DataAccess;
using HospitalSimConsole.BusinessLogic;
using HospitalSimConsole.DataModel;

namespace HospitalSim.Test
{
    [TestClass]
    public class ConsultationSchedulerTest
    {
        private TestHelper _testHelper;
        private MapToViewModel _map;
        private Patient _patientWithFlu;
        private Patient _patientWithBreastCancer;
        private Patient _patientWithHeadNeckCancer;
        private ConsultationScheduler _scheduler;

        [TestInitialize]
        public void Init()
        {
            _testHelper = new TestHelper();
            _map = _testHelper.LoadHospitalResourcesAndMapToModelForTest();
            _patientWithFlu = _testHelper.CreateFluPatient();
            _patientWithBreastCancer = _testHelper.CreateCancerPatientWithTopologyBreast();
            _patientWithHeadNeckCancer = _testHelper.CreateCancerPatientWithTopologyHeadAndNeck();
            _scheduler = new ConsultationScheduler(_map);
                    }

        [TestMethod]
        public void VerifyConsultationDateIsLaterThanRegistrationDate()
        {
            var consultation = _scheduler.CreateConsultation(new Patient { Name = "FluPatient", Condition = new Condition { ConditionType = ConditionType.Flu, Topology = Topology.None } });
            int result = DateTime.Compare(consultation.ConsultationDate, consultation.RegistrationDate);
            Assert.IsTrue(result> 0);
        }

        [TestMethod]
        public void VerifyFluPatientGetsGeneralPractioner()
        {
            var patient = _patientWithFlu;
            var consultation = _scheduler.CreateConsultation(patient);
            Assert.IsTrue(consultation.Doctor.Roles.Contains(Role.GeneralPractitioner));
        }

        [TestMethod]
        public void VerifyBreastCancerPatientMeetsOncologistInRoomWithAdvancedOrSimpleMachine()
        {
            var patient = _patientWithBreastCancer;
            var consultation = _scheduler.CreateConsultation(patient);
            Assert.IsTrue(consultation.Doctor.Roles.Contains(Role.Oncologist));
            Assert.IsTrue(consultation.TreatmentRoom.hasMachine);          
        }

        [TestMethod]
        public void VerifyCancerHeadNeckPatientMeetOncologistInRoomWithAdvancedMachine()
        {
            var patient = _patientWithHeadNeckCancer;
            var consultation = _scheduler.CreateConsultation(patient);
            Assert.IsTrue(consultation.Doctor.Roles.Contains(Role.Oncologist));
            Assert.IsTrue(consultation.TreatmentRoom.hasMachine);
            Assert.IsTrue(consultation.TreatmentRoom.TreatmentMachine.Capability == Capability.Advanced);            
        }       
    }
}
