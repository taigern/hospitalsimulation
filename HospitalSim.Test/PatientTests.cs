﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HospitalSimConsole.DataAccess;
using HospitalSimConsole.DataModel;

namespace HospitalSim.Test
{
    [TestClass]
    public class PatientTests
    {
        [TestMethod]
        public void ImportPatienListTest()
        {
            // Arrange
            var patients = InitializeService.RegisterPatients();

            //Act

            // Assert
            Assert.IsTrue(patients.Exists(p => p.Condition.ConditionType.Equals(ConditionType.Cancer)));            
        }
    }
}
