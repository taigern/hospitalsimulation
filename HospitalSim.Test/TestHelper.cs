﻿using HospitalSimConsole.BusinessLogic;
using HospitalSimConsole.DataAccess;
using HospitalSimConsole.DataModel;
using System.Collections.Generic;

namespace HospitalSim.Test
{
    public class TestHelper
    {
        public MapToViewModel LoadHospitalResourcesAndMapToModelForTest()
        {
            InitializeService.LoadHospitalResources();
            var docs = InitializeService.doctors;
            var rooms = InitializeService.treatmentRooms;
            var machines = InitializeService.treatmentMachines;
            return new MapToViewModel(docs, rooms, machines);
        }

        public IEnumerable<DoctorViewModel> CreateListOfDoctorViewModel()
        {
            var doctors = new List<DoctorViewModel>
            {
                new DoctorViewModel
                {
                    Name = "John",
                Roles = new List<Role>{Role.Oncologist },
                Consultations = new List<Consultation>()
                },
                new DoctorViewModel
                {
                    Name = "Anna",
                    Roles = new List<Role>{Role.GeneralPractitioner},
                    Consultations = new List<Consultation>()
                } ,
                new DoctorViewModel{
                    Name = "Peter",
                    Roles = new List<Role>{Role.Oncologist, Role.GeneralPractitioner},
                    Consultations = new List<Consultation>()
                }
            };
            return doctors;

        }

        public IEnumerable<TreatmentRoomViewModel> CreateListOfTreatmentRoomsWithMachines()
        {
            var rooms = new List<TreatmentRoomViewModel>()
            {
                new TreatmentRoomViewModel
                {
                    Name = "One",
                    hasMachine = true,
                    TreatmentMachine = new TreatmentMachineViewModel
                    {
                        Name = "Elekta",
                        Capability = Capability.Advanced
                    }

                },
                new TreatmentRoomViewModel
                {
                    Name = "Two",
                    hasMachine = true,
                    TreatmentMachine = new TreatmentMachineViewModel
                    {
                        Name = "Varian",
                        Capability = Capability.Advanced
                    }
                },
                new TreatmentRoomViewModel
                {
                    Name = "Three",
                    hasMachine = true,
                    TreatmentMachine = new TreatmentMachineViewModel
                    {
                        Name ="MM50",
                        Capability = Capability.Simple
                    }
                },
                new TreatmentRoomViewModel
                {
                    Name = "Four",
                    hasMachine = false
                },
                new TreatmentRoomViewModel
                {
                    Name = "Five",
                    hasMachine = false
                }
            };
            return rooms;
        }


        public Patient CreateCancerPatientWithTopologyBreast()
        {
            return CreatePatient("BreastCancer", ConditionType.Cancer, Topology.Breast);
        }
        public Patient CreateCancerPatientWithTopologyHeadAndNeck()
        {
            return CreatePatient("HeadNeckCancer", ConditionType.Cancer, Topology.HeadAndNeck);
        }
        public Patient CreateFluPatient()
        {
            return CreatePatient("FluPatient",ConditionType.Flu, Topology.None);
        }

        public Patient CreatePatient(string name, ConditionType type, Topology topology)
        {
            return new Patient { Name = name, Condition = new Condition() { ConditionType = type, Topology = topology } };
        }

    }
}

