﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HospitalSimConsole.DataAccess;
using System.Linq;
using HospitalSimConsole.DataModel;
using HospitalSimConsole.BusinessLogic;
using System.Collections.Generic;

namespace HospitalSim.Test
{
    [TestClass]
    public class ServiceInitTest
    {
        TestHelper _testHelper;
        private MapToViewModel _map;
        private IEnumerable<TreatmentRoomViewModel> _testTreatmentRooms;

        [TestInitialize]
        public void Init()
        {
            _testHelper = new TestHelper();
            _map = _testHelper.LoadHospitalResourcesAndMapToModelForTest();
            _testTreatmentRooms = _testHelper.CreateListOfTreatmentRoomsWithMachines();

        }

        [TestMethod]
        public void ImportHospitalResourcesTest()
        {
            // Arrange
            InitializeService.LoadHospitalResources();

            //Act

            // Assert
            Assert.AreEqual(3, InitializeService.doctors.Count);
            Assert.AreEqual(3, InitializeService.treatmentMachines.Count);
            Assert.AreEqual(5, InitializeService.treatmentRooms.Count);
        }

        

        // The value of this test can be discussed...
        [TestMethod]
        public void MappingDoctorsTest()
        {
            
            var doctors = _testHelper.CreateListOfDoctorViewModel();
            
            foreach (var doc in _map.doctorsVM)
            {
                Assert.IsTrue(doctors.ToList().Exists(n => n.Name == doc.Name));
            }

            

        }
        [TestMethod]
        public void MappingGivesCorrectTreatmentRoomsCount()
        {            
            Assert.IsTrue(_testTreatmentRooms.Count() == _map.treatmentRoomsVM.Count());
        }
        [TestMethod]
        public void MappingGivesCorrectNumberOfRoomsWithoutMachine()
        { 
            var emptyRooms = _map.treatmentRoomsVM.Where(r => r.hasMachine == false);
            Assert.IsTrue(emptyRooms.Count() == (_testTreatmentRooms.Where(tr=>tr.hasMachine==false)).Count());         


        }
    }
        
}
