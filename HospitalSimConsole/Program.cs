﻿using HospitalSimConsole.BusinessLogic;
using HospitalSimConsole.DataAccess;
using HospitalSimConsole.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using static HospitalSimConsole.DataModel.HospitalResourceModel;

namespace HospitalSimConsole
{
    class Program
    {
        private static List<TreatmentMachine> machines;
        private static List<TreatmentRoom> treatmentRooms;
        private static List<Doctor> doctors;
        private static List<Patient> patients;

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome!");
            Console.WriteLine();
            Console.WriteLine("This application simulates a hospital and starts by loading resources.");
            SleepForASec();
            Console.WriteLine();
            GetHospitalResources();            
            
            Console.Write("Registration of patients begins in ");           
            for (int a = 3; a >= 0; a--)
            {
                Console.CursorLeft = 35;
                Console.Write("{0} ", a);    // Add space to make sure to override previous contents
                SleepForASec();
            }

            Console.WriteLine();  
            ScheduleConsultations();                      
            Console.ReadLine();
        }

        private static void ScheduleConsultations()
        {            
            patients = InitializeService.RegisterPatients();
            var map = new MapToViewModel(doctors, treatmentRooms, machines);
            var scheduler = new ConsultationScheduler(map);
            List<Consultation> consultations = new List<Consultation>();
            Console.WriteLine();
            foreach (var p in patients)
            {
                var consultation = scheduler.CreateConsultation(p);
                Console.WriteLine(consultation.ToString());
                Console.WriteLine();
                SleepForASec();
            }
        }              

        private static void GetHospitalResources()
        {
            InitializeService.LoadHospitalResources();
            doctors = InitializeService.doctors;
            treatmentRooms = InitializeService.treatmentRooms;
            machines = InitializeService.treatmentMachines;

            
            Console.WriteLine("The hospital has " + doctors.Count() + " doctors, " + machines.Count() + " treatmentMachines and " + treatmentRooms.Count() + " treatmentRooms.");
            Console.WriteLine();
        }

        private static void SleepForASec()
        {
            System.Threading.Thread.Sleep(1000);
        }
    }

}
