﻿using HospitalSimConsole.DataModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using static HospitalSimConsole.DataModel.HospitalResourceModel;

namespace HospitalSimConsole.DataAccess
{
    public static class InitializeService
    {

        public static List<Doctor> doctors;
        public static List<TreatmentMachine> treatmentMachines;
        public static List<TreatmentRoom> treatmentRooms;
        public static List<Patient> patients;

        public static void LoadHospitalResources()
        {
            const string resFileName = "HospitalInput.json";
            //Uri filename = new Uri(resFileName, UriKind.Relative);

            var rootArray = ReadJsonFile(resFileName);// JObject.Parse(File.ReadAllText(filename.ToString()));

            var doctorsArray = (JArray)rootArray["doctors"];
            var treatmentMachinesArray = (JArray)rootArray["treatmentMachines"];
            var treatmentRoomsArray = (JArray)rootArray["treatmentRooms"];

            doctors = (List<Doctor>)doctorsArray.ToObject<IList<Doctor>>();
            treatmentMachines = (List<TreatmentMachine>)treatmentMachinesArray.ToObject<IList<TreatmentMachine>>();
            treatmentRooms = (List<TreatmentRoom>)treatmentRoomsArray.ToObject<IList<TreatmentRoom>>();


        }

        public static JObject ReadJsonFile(string fname)
        {
            Uri filename = new Uri(fname, UriKind.Relative);
            return JObject.Parse(File.ReadAllText(filename.ToString()));
        }

        public static List<Patient> RegisterPatients()
        {
            const string patientFilename = "PatientInput.json";
            var rootArray = ReadJsonFile(patientFilename);

            var patientArray = (JArray)rootArray["patients"];

            return (List<Patient>)patientArray.ToObject<IList<Patient>>();

            
            //var patient = new Patient { Name="Marge", Condition = new Condition { ConditionType = ConditionType.Flu, Topology = Topology.None } };
            //new ConsultationScheduler().CreateConsultation(patient);
        }
    }
}
