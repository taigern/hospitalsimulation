﻿using System.Collections.Generic;

namespace HospitalSimConsole.DataModel
{
    public class TreatmentRoomViewModel
    {
        public string Name { get; set; }
        public bool hasMachine { get; set; } // if false use for flu patients
        public TreatmentMachineViewModel TreatmentMachine { get; set; }        
        public List<Consultation> Consultations { get; set; }

        public override string ToString()
        {
            if (hasMachine)
            {
                return Name + " that has a \ntreatment machine with " + TreatmentMachine.Capability + " capability.";
            }
            return Name + ".";

        }
    }

    public class TreatmentMachineViewModel
    {
        public string Name { get; set; }
        public Capability Capability { get; set; }
    }
    public enum Capability
    {
        Simple, Advanced, None
    }
}
