﻿using System.Collections.Generic;

namespace HospitalSimConsole.DataModel
{
    public class HospitalResourceModel
    {
        public class Doctor
        {
            public string Name { get; set; }
            public List<string> Roles { get; set; }
        }

        

        public class TreatmentMachine
        {
            public string Name { get; set; }
            public string Capability { get; set; }
        }

        public class TreatmentRoom
        {
            public string Name { get; set; }
            public string TreatmentMachine { get; set; }
        }

        public class RootObject
        {
            public List<Doctor> Doctors { get; set; }
            public List<TreatmentMachine> TreatmentMachines { get; set; }
            public List<TreatmentRoom> TreatmentRooms { get; set; }
        }
    }
}
