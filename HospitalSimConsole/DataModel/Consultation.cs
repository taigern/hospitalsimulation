﻿using System;
using System.Linq;

namespace HospitalSimConsole.DataModel
{
    public class Consultation
    {
        public Patient Patient { get; set; }
        public DoctorViewModel Doctor { get; set; }
        public TreatmentRoomViewModel TreatmentRoom { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime ConsultationDate { get; set; }

        public override string ToString()
        {

            return Patient.ToString() + " has been scheduled for a consultation \nwith " 
                + Doctor.Roles.FirstOrDefault().ToString() + " " + Doctor.Name + " on " + ConsultationDate.ToShortDateString()
                + " in treatment room " + TreatmentRoom.ToString();
            
        }
    }
}
