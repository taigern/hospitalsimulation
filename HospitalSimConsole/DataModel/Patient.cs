﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace HospitalSimConsole.DataModel
{
    public class Patient
    {        
        public string Name { get; set; }
        public Condition Condition { get; set; }

        public override string ToString()
        {
            if (this.Condition.Topology.Equals(Topology.None))
            {
                return "Patient " + this.Name + " with " + this.Condition.ConditionType.ToString();    
                }
            return "Patient " + this.Name + " with " + this.Condition.Topology.ToString() + " " + this.Condition.ConditionType.ToString();
        }
    }   

    public class Condition
    {
        [JsonProperty("conditiontype")]
        [JsonConverter(typeof(StringEnumConverter))]
        public ConditionType ConditionType { get; set; }

        [JsonProperty("topology")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Topology Topology { get; set; }
    }

    
    public enum ConditionType
    {       
        Cancer,
        Flu
    }
    public enum Topology
    {
        None, Breast, HeadAndNeck
    }
}
