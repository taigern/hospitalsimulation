﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HospitalSimConsole.DataModel
{
    public class DoctorViewModel
    {
        public string Name { get; set; }
        public List<Role> Roles { get; set; }

        public List<Consultation> Consultations { get; set; }
    }
    public enum Role
    {
        Oncologist, GeneralPractitioner
    }
}
