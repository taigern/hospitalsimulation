﻿using HospitalSimConsole.DataModel;
using System.Collections.Generic;
using static HospitalSimConsole.DataModel.HospitalResourceModel;

namespace HospitalSimConsole.BusinessLogic
{
    public class MapToViewModel
    {        
        public List<DoctorViewModel> doctorsVM = new List<DoctorViewModel>();
        public List<TreatmentRoomViewModel> treatmentRoomsVM = new List<TreatmentRoomViewModel>();
        public MapToViewModel(List<Doctor> doctors, List<TreatmentRoom> treatmentRooms, List<TreatmentMachine> treatmentMachines)
        {
            MapDoctors(doctors);
            MapTreatmentRoomsAndAddMachines(treatmentRooms, treatmentMachines);        
        }

        private void MapTreatmentRoomsAndAddMachines(List<TreatmentRoom> treatmentRooms, List<TreatmentMachine> treatmentMachines)
        {            
            foreach (var r in treatmentRooms)
            {
                var tr = new TreatmentRoomViewModel()
                {
                    Name = r.Name,
                    hasMachine = false,
                    Consultations = new List<Consultation>()
                };
                if (!string.IsNullOrEmpty(r.TreatmentMachine))
                {
                    tr.hasMachine = true;
                    foreach (var m in treatmentMachines)
                    {
                        if (m.Name.Equals(r.TreatmentMachine))
                        {
                            tr.TreatmentMachine = new TreatmentMachineViewModel()
                            {
                                Name = m.Name,
                                Capability = GetMachineCapability(m)
                            };
                        }                        
                    }
                }
                treatmentRoomsVM.Add(tr);
            }
        }

        private Capability GetMachineCapability(TreatmentMachine m)
        {
            if (m.Capability.Equals("Advanced"))
            {
                return Capability.Advanced;
            }           
            return Capability.Simple;            
        }

        private void MapDoctors(List<Doctor> doctors)
        {
            foreach (var doc in doctors)
            {
                var roles = new List<Role>();
                foreach (var role in doc.Roles)
                {
                    if (role.Equals("Oncologist"))
                    {
                        roles.Add(Role.Oncologist);
                    }
                    if (role.Equals("GeneralPractitioner"))
                    {
                        roles.Add(Role.GeneralPractitioner);
                    }
                }
                doctorsVM.Add(new DoctorViewModel
                {
                    Name = doc.Name,
                    Roles = roles,
                    Consultations = new List<Consultation>()
                });
            }
        }
    }
}
