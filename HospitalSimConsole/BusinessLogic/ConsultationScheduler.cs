﻿using System;
using HospitalSimConsole.DataModel;
using System.Collections.Generic;
using System.Linq;

namespace HospitalSimConsole.BusinessLogic
{
    public class ConsultationScheduler
    {
        private IEnumerable<DoctorViewModel> _doctors;
        private IEnumerable<TreatmentRoomViewModel> _rooms;       
        private List<Capability> _neededCapabilities;
        private Role _doctorRole;
        private bool _needsMachine;
        private Consultation _scheduledConsultation;

        public ConsultationScheduler(MapToViewModel map)
        {
            _doctors = map.doctorsVM;
            _rooms = map.treatmentRoomsVM;
        }
        public Consultation CreateConsultation(Patient patient)
        {
            _scheduledConsultation = new Consultation()
            {
                Patient = patient,
                RegistrationDate = DateTime.Today
            };                    
                
            SetPatientConditionTypeAndTopology(patient.Condition);
            var roomOptions = GetTreatmentRooms();   
            var docOptions = _doctors.Where(d => d.Roles.Contains(_doctorRole));

            SetDateForConsultation(docOptions, roomOptions);       
                     
            return _scheduledConsultation;
        }

        private IEnumerable<TreatmentRoomViewModel> GetTreatmentRooms()
        {            
            if (!_needsMachine)
            {
                return _rooms.Where(r => !r.hasMachine);
            }
            return _rooms.Where(r => r.hasMachine && HasNeededCapability(r.TreatmentMachine));            
        }

        private void SetDateForConsultation(IEnumerable<DoctorViewModel> docOptions, IEnumerable<TreatmentRoomViewModel> roomOptions)
        {
            var tryDate = _scheduledConsultation.RegistrationDate;

            bool incrDay = true;
            while (incrDay)
            {
                tryDate = tryDate.AddDays(1);
                foreach (var doc in docOptions)
                {
                    if (IsAvailableOnDate(doc.Consultations, tryDate))
                    {
                        foreach (var room in roomOptions)
                        {
                            if (IsAvailableOnDate(room.Consultations, tryDate))
                            {
                                incrDay = false;
                                _scheduledConsultation.TreatmentRoom = room;
                                _scheduledConsultation.Doctor = doc;
                                _scheduledConsultation.ConsultationDate = tryDate;
                                room.Consultations.Add(_scheduledConsultation);
                                doc.Consultations.Add(_scheduledConsultation);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
        /// <summary>
        /// Determine what resources are needed based on the condition type and topology.
        /// </summary>
        private void SetPatientConditionTypeAndTopology(Condition condition)
        {
            _neededCapabilities = new List<Capability>();
            if (condition.ConditionType == ConditionType.Cancer)
            {
                _doctorRole = Role.Oncologist;
                _needsMachine = true;

                if (condition.Topology == Topology.Breast)
                {
                    _neededCapabilities.Add(Capability.Simple);
                }
                else
                {
                    _neededCapabilities.Add(Capability.Advanced);
                }
            }
            else
            {
                _neededCapabilities.Add(Capability.None);
                _doctorRole = Role.GeneralPractitioner;
                _needsMachine = false;
            }
        }
        
        private bool IsAvailableOnDate(List<Consultation> consultations, DateTime date)
        {                    
            if (consultations == null)
            {
                return true;
            }
            return !consultations.Exists(dt => dt.ConsultationDate.Day.Equals(date.Day));            
        }

        private bool HasNeededCapability(TreatmentMachineViewModel machine)
        {            
            foreach (var cap in _neededCapabilities)
            {
                if (machine.Capability == cap)
                {
                    return true;
                } 
            }
            return false;
        }
    }
}
